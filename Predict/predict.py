import requests

def get_url(x,y,x1,y1):
    """Function giving the API URL to get IGN pictures from the coordinates of the bottom left and top right points.

    Args:
        x (float): latitude of the bottom-left point
        y (float): longitude of the bottom-left point
        x1 (float): latitude of the top-right point
        y1 (float): longitude of the top-right point

    Returns:
        url (str): url to get the IGN photography 
    """
    url = "https://wxs.ign.fr/essentiels/geoportail/r/wms?LAYERS=ORTHOIMAGERY.ORTHOPHOTOS&EXCEPTIONS=text/xml&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&CRS=EPSG:2154&WIDTH=400&HEIGHT=400"
    urlfinal=url+f"&BBOX={x},{y},{x1},{y1}"
    return urlfinal

def get_predict (xini, yini,model):
    result=0
    for i in range(0,284):
        for j in range(0,330):
            x=xini + j*80
            y=yini+i*80
            x1=xini+(j+1)*80
            y1=yini+(i+1)*80

            url = get_url(x,y,x1,y1)
            response=requests.get(url,stream=True)
            if response.status_code==200:
                with open(f'data/image_predict.png',"wb") :
                    image_path = "data/image_predict.png"
                    boxes = model.predict_image(path=image_path, return_plot = False)
                    result = result+len(boxes)
    return result




# #ville de paris :
# Nord : Saint-Denis 
# 6870731.86
# 652890.88
# # Ouest : Chanille
# 6856817.92
# 640700.14
# # Sud : Orly 
# 6847913.03
# 653778.51
# # Est : Noisy-le-grand
# 6860881.81
# 667213.70

#Coordonnées extrèmes paris : 640700.14,6847913.03,667213.70,6870731.86
#nombre d'itérations/taille du mappage : 667213.70-640700.14/80 images sur 6870731.86-6847913.03/80 => 330/285