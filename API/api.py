from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import List
from fastapi import FastAPI #, File, HTTPException, UploadFile
import matplotlib.pyplot as plt
from deepforest import main
from PIL import Image

# # bloc ci-dessous décomenté lors du fonctionnement de l'api alexandre
# model = load_model()

model = main.deepforest.load_from_checkpoint("model/Dataset2/checkpoint.pl")
model.score_thresh = 0.3

#Define the response JSON
class Prediction(BaseModel):
    filename: str
    content_type: str
    predictions: List[dict] = []


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allows all origins
    allow_credentials=True,
    allow_methods=["*"],  # Allows all methods
    allow_headers=["*"])  # Allows all headers

@app.get("/test")
def test():
    return "coucou"

@app.get("/test-operation-bidon")
def test_operation_bidon(entered_data):
    return (f"{int(entered_data)*2} millions de papillons")


@app.get("/predict-image")
def predict_image(url):
    # pkl_file = open(url, 'rb')
    # image = pickle.load(pkl_file)
    # im = plt.imread(image)
    # image = prepare_image(im)
    boxes = model.predict_image(path=url, return_plot = True)
    img=Image.fromarray(boxes,'RGB')
    img.save("data/predicted_image.png")
    plt.imshow(boxes[:,:,::-1])
    plt.show()

def get_predict (xini,yini):
    result=0
    for i in range(0,284):
        for j in range(0,330):
            x=xini + j*80
            y=yini+i*80
            x1=xini+(j+1)*80
            y1=yini+(i+1)*80

            url = get_url(x,y,x1,y1)
            response=requests.get(url,stream=True)
            if response.status_code==200:
                with open(f'data/image_predict.png',"wb") :
                    image_path = "data/image_predict.png"
                    boxes = model.predict_image(path=image_path, return_plot = False)
                    result = result+len(boxes)
    return result