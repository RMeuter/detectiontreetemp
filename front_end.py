import streamlit as st
from PIL import Image
import time
import requests


#CSS

CSS = """
.titre_principal {
    text-align: center; color : white; FONT face='century gothic';
}
"""
st.write('<style>{CSS}</style>', unsafe_allow_html=True)

#titres et textes introductifs
logo_snapillon = Image.open("images_streamlit/logo.jfif")

col1, col2, col3 = st.columns(3)
with col1:
    pass
with col2:
    st.image(logo_snapillon )
with col3 :
    pass

#st.markdown("<h1 class='titre_principal'>SNAPILLON</h1>", unsafe_allow_html=True)
st.markdown("<h1 style='text-align: center; color: black;'>Par ici les Photos !</h1>", unsafe_allow_html=True) 
st.markdown("""### Uploader une photo afin d'identifier les arbres présents
""")

# #set background

# @st.cache
# def load_image(path):
#     with open(path, 'rb') as f:
#         data = f.read()
#     encoded = base64.b64encode(data).decode()
#     return encoded

# def image_tag(path):
#     encoded = load_image(path)
#     tag = f'<img src="data:image/png;base64,{encoded}">'
#     return tag

# def background_image_style(path):
#     encoded = load_image(path)
#     style = f'''
#     <style>
#     .stApp {{
#         background-image: url("data:image/png;base64,{encoded}");
#         background-size: cover;
#     }}
#     </style>
#     '''
#     return style
# image_path = "images_streamlit/background1.png"

# st.write(background_image_style(image_path), unsafe_allow_html=True)


#drag and drop files
st.set_option('deprecation.showfileUploaderEncoding', False)

uploaded_file = st.file_uploader("", type=['png','jpeg','jpg'])

if uploaded_file is not None:
    #file_details = {"FileName":uploaded_file.name,"FileType":uploaded_file.type,"FileSize":uploaded_file.size}
    #st.write(file_details)
    image = Image.open(uploaded_file)
    image.save("data/saved_image.jpg")
    col1, col2 = st.columns(2)
    with col1:
        st.image(image)
    with col2:
        st.warning('Photo chargée avec succès !')
        st.markdown("""### *Vous pouvez lancer une nouvelle prédiction en glissant une nouvelle photo ou bien en fermant avec la croix*
        """)
    st.markdown("""## ... Lancement de l'algorithme Snapillon ... """)
    latest_iteration = st.empty()
    bar = st.progress(0)

    for i in range(100):
        # Update the progress bar with each iteration.
        latest_iteration.text(f'Iteration {i+1}')
        bar.progress(i + 1)
        time.sleep(0.02)

    st.markdown("""# Par ici les résultats !
    """)
    
    parameters2 = dict(url = "data/saved_image.jpg")
    url2 = 'http://127.0.0.1:8000/predict-image'
    dico = requests.get(url2, params = parameters2).json()

    predicted_image = Image.open("data/predicted_image.png")
    st.image(predicted_image)


    # st.markdown("""# Combien d'arbres dans ma ville?""")
    # st.markdown("""### EX : Paris!""")
    # parameters3={"xini":640700.14,"yini":6847913.03}
    # url3 = 'http://127.0.0.1:8000/get_predict'
    # dico = requests.get(url3,params=parameters3).json()
    # st.markdown(dico)