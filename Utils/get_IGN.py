import random
import requests
import shutil

def get_coord(x,y):
    """Function getting a point (lat,long) and return a couple of points : 
    the one entered (bottom left) and the one to get a picture of 80m/80m (top right) in a system epsg = 2154

    Args:
        x (float): latitude of the original point
        y (float): longitude of the original point

    Returns:
        Tuple: (x,y,x1,y1) with x1 (float) and y1 (float) the latitude and longitude of the top right point
    """
    x1 = float(x)+80
    y1 = float(y)+80
    print(f'{x},{y},{x1},{y1}')
    return (x,y,x1,y1)
    #print(f'{x},{y},{x2},{y2}')
    
def get_url(x,y,x1,y1):
    """Function giving the API URL to get IGN pictures from the coordinates of the bottom left and top right points.

    Args:
        x (float): latitude of the bottom-left point
        y (float): longitude of the bottom-left point
        x1 (float): latitude of the top-right point
        y1 (float): longitude of the top-right point

    Returns:
        url (str): url to get the IGN photography 
    """
    url = "https://wxs.ign.fr/essentiels/geoportail/r/wms?LAYERS=ORTHOIMAGERY.ORTHOPHOTOS&EXCEPTIONS=text/xml&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&CRS=EPSG:2154&WIDTH=400&HEIGHT=400"
    urlfinal=url+f"&BBOX={x},{y},{x1},{y1}"
    return urlfinal

def get_images(url,image_name):
    """Functio to get and save locally the ign image from its url

    Args:
        url (str): url of the IGN picture
        image_name (str): name of the image saved locally
    """
    response=requests.get(url,stream=True)
    if response.status_code==200:
        with open(f'data/{image_name}.png',"wb") as fil :
            response.raw.decode_content=True
            shutil.copyfileobj(response.raw,fil)
    # resource = request.urlopen(url)
    # output = open(f"data/{image_name}.png","wb")
    # #output.write(resource.read())
    # output.close()


if __name__ == "__main__":
    for i in range(1,21):
        x=random.randint(891006,903100)
        y=random.randint(6239170,6254740)
        (x,y,x1,y1)=get_coord(x,y)
        url = get_url(x,y,x1,y1)
        get_images(url,image_name=f"images{i}")
    #893103.70,6242009.21
    #43.28073193983449, 5.3946874413138755
    
    #bas_gauche : 891006.6,6239170.3
    #haut-droite : 903100,6254740