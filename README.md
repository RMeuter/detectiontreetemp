# DetectionTreeTemp


## Objectif : 

L'object de ce projet est d'améliorer l'existant, afin de pouvoir avoir de meilleur performance de prédiction.

### Détail sur l'existant : 

Actuellement ce projet a deux sous versions créer par des groupes antérieurs : 
- Une durant un stage de 6 mois: [Source](https://github.com/aloui-mathias/ecoTeka-deepforest-demo)
- Une autre par un groupe d'étudiant du vagon: [Source](https://github.com/HerySon/Hack4Nature)

Ces deux projets se base sur l'algorithme issue du package : [deep forest](https://deepforest.readthedocs.io/en/latest/)

## Stratégie

Comme la documentation le conseil, nous avons réentrainer le modèle sur images d'arbre de ville. 

## Déroulement :

Ce projet s'est déroulée en 5 temps :
1. Recherche de source de donnée 
2. Collecte des données,
3. Labelisation,
4. Entrainnement et selection du meilleur candidate (modèle),
5. Création d'un site web

## 1/ Recherche de source de donnée 

Cette permière étape 

Url pour la récupération de raster: 
**Url:** https://geoservices.ign.fr/services-web-essentiels

### Exemple 
https://wxs.ign.fr/essentiels/geoportail/r/wms?LAYERS=ORTHOIMAGERY.ORTHOPHOTOS&EXCEPTIONS=text/xml&FORMAT=image/tif&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&CRS=EPSG:4326&BBOX=47.34956960,3.25167353,47.38545104,3.30486151&WIDTH=400&HEIGHT=400

**Plus d'information:** https://geoservices.ign.fr/documentation/services/api-et-services-ogc/images-wms-ogc


## 2/ Collecte des données

Les données ont été recupérer de la ville de Marseille via la requete issue de l'IGN : 100 images ont été prises aléatoirement dans un rectangle couvrant la ville pour l'entrainnement, 23 images ont servis de test. 

## 3/ Labelisation

La labelisation s'est effectuée via le package: https://labelstud.io/

## 4/ Entrainnement et selection du meilleur candidate (modèle)

L'entrainnement du modèles s'est effectué en selectionnant les différents candidats entrainer selon les différents datasets:
- Dataset 0: Le modèle actuel,
- Dataset 1: Modèle réentrainné sur les données dans le projet du repo "Heryson" (Le modèle n'avait pas montrer de différence avec l'actuel selon les membres de ce projet donc nous n'avons pas retesté ce modèle),
- Dataset 2: Modèle réentrainné sur les dataset 1 et 3,
- Dataset 3: Modèle réentrainné sur les données labelisé durant le hackathon,

L'entrainnement du modèle s'est effectué selon les mêmes paramètre que le modèle actuelle, nous avons effectué 4 epoch pour l'entrainnement et le batch size est de 1.

La selection s'est effectué selon le critère répondant le plus au besoin c'est à dire la précision, où nous voulons à l'avenir plus sous estimer le nombre d'arbre sur une carte que l'inverse. 

Le candidat retenu est le modèle du dataset 2, ayant augmenter de 0.21 en score de précision et de 0.4 en recall par rapport au modèle originale.

## 5/ Création d'un site web

Le site crée pour le hackathon a été fait pour montrer la performance du modèle du dataset 2. Le framework utilisé est [streamlite](https://streamlit.io/)

## Perspectve et réflexion:

A l'avenir, la labelisation pour être faite à partir des couches infra Rouge au lieu d'image RGB actuel, il peut servir à mieux detecter la nature via le calcul suivant : [ndvi](https://en.wikipedia.org/wiki/Normalized_difference_vegetation_index).

A partir du projet Heryson, nous pouvons remarquer que la précision des images IGN sont nécéssaires pour la bonne detection des arbres dans un milieu urbain.
